import { Component, Input, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { RutasService } from '../services/rutas.service';

@Component({
  selector: 'app-puntocomsumo',
  templateUrl: './puntocomsumo.component.html',
  styles: [
  ]
})
export class PuntocomsumoComponent implements OnInit, AfterViewInit {

  @Input("agregar")
  agregarACampana: boolean;

  @Input("punto")
  punto: any;

  puntoConsumo = {
    direccion: '',
    actividad: '',
    estrato: '',
    imagen: null,
  }
  infoCodigo = [];

  isLoading = false;

  constructor(
    private rutasService: RutasService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.rutasService.getPreseleccionPuntos(this.punto.CodigoPuntoConsumo).subscribe(
      (result) => {
        this.infoCodigo = result;
        console.log(result);
        if(result.length > 0) {
          this.puntoConsumo.direccion = result[0].DireccionPuntoConsumo;
          this.puntoConsumo.actividad = result[0].DESCR_CORTA;
          this.puntoConsumo.estrato = result[0].CodigoEstrato;
          this.puntoConsumo.imagen = result[0].image;
        }
        this.isLoading = false;
      },
      (error) => {
        console.error(error);
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {
  }
}
