import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ReglasService } from '../services/reglas.service';

@Component({
  selector: 'app-reglas',
  templateUrl: './reglas.component.html',
  styleUrls: ['./reglas.component.css']
})
export class ReglasComponent implements OnInit {

  Soporte: number;
  Confianza: number;
  Actividad = { Division: 'TODOS', DESCR_CORTA: 'TODOS' };
  Estrato = { CodigoEstrato: 'TODOS'};

  EstratosList = [];
  ActividadesList = [];

  ReglasList = [];

  formRegla = {
    Nombre: '',
    HallazgoA: null,
    HallazgoB: null,
    Descripcion: '',
    Actividad: { Division: 'TODOS', DESCR_CORTA: 'TODOS' },
    Estrato: { CodigoEstrato: 'TODOS'}
  }

  // HallazgosList= [];
  TipoAnomaliasList = [];
  TipoAnomaliaA: any;
  TipoAnomaliaB: any;
  AnomaliasAList = [];
  AnomaliasBList = [];

  generacionErrores = false;
  generacionLoading = false;

  @ViewChild('closeModal') closeModal: ElementRef;
  crearError = false;

  constructor(
    private reglasService: ReglasService
  ) { }

  async ngOnInit() {
    this.ActividadesList = await this.reglasService.getActividades().toPromise();
    this.EstratosList = await this.reglasService.getEstratos().toPromise();

    this.obtenerReglas();
    // this.reglasService.getHallazgos().subscribe(
    //   (result) => {
    //     this.HallazgosList = result;
    //     this.formRegla.HallazgoA = result[0];
    //     this.formRegla.HallazgoB = result[0];
    //   }
    // );
    this.reglasService.getHallazgosTipoAnomalias().subscribe( 
      (result) => {
        this.TipoAnomaliasList = result;
        this.TipoAnomaliaA = result[0];
        this.onChangeHallazgoA();
        this.TipoAnomaliaB = result[0];
        this.onChangeHallazgoB();
      }
    );
  }

  obtenerReglas() {
    this.reglasService.getReglas().subscribe(
      (result) => {
        this.ReglasList = result;
      }
    );
  }

  generacionAutomatica( formGeneracion: NgForm) {
    this.generacionErrores = false;
    this.generacionLoading = true;

    if(formGeneracion.invalid) {
      this.generacionErrores = true;
      this.generacionLoading = false;
      return;
    }

    // console.log(this.Soporte, ' - ', this.Confianza, ' - ', this.Estrato, ' - ', this.Actividad, ' - ');
    const reqBody = {
      Soporte: this.Soporte,
      Confianza: this.Confianza,
      ActividadEconomica: this.Actividad.DESCR_CORTA,
      Estrato: this.Estrato.CodigoEstrato
    }

    this.reglasService.postGeneracionAutomatica(reqBody).subscribe( 
      (result) => {
        this.ReglasList = result;
        this.generacionLoading = false;
      },
      (error) => {
        console.error(error);
        this.generacionLoading = false;
      }
    );
  }

  closeCrearRegla() {
    this.formRegla = {
        Nombre: '',
        HallazgoA: null,
        HallazgoB: null,
        Descripcion: '',
        Actividad: { Division: 'TODOS', DESCR_CORTA: 'TODOS' },
        Estrato: { CodigoEstrato: 'TODOS'}
      };
  }

  generarRegla( regla: any, value: number ) {
    const req = {
      Nombre: regla.NombreRegla,
      Generada: value
    }
    this.reglasService.putGenerar(req).subscribe(
      (result) => {
        console.log('success update');
        regla.Generada = value;
      }
    );
  }

  eliminarRegla( regla) {
    regla.eliminarLoading = true;
    this.reglasService.deleteRegla(regla.NombreRegla).subscribe(
      (result) => {
        console.log('Delete exitoso');
        this.obtenerReglas();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  crearRegla() {
    this.crearError = false;
    console.log(this.formRegla);
    if(!this.formRegla.Nombre) {
      this.crearError = true;
      return;
    }

    const req = {
      Nombre: this.formRegla.Nombre,
      HallazgoA: this.TipoAnomaliaA.TipoAnomalias !== 'Sin Definición' ? this.formRegla.HallazgoA.NombreAnomalias:'Sin Definición',
      HallazgoB: this.TipoAnomaliaB.TipoAnomalias !== 'Sin Definición' ? this.formRegla.HallazgoB.NombreAnomalias:'Sin Definición',
      Descripcion: this.formRegla.Descripcion,
      ActividadEconomica: this.formRegla.Actividad.DESCR_CORTA,
      Estrato: this.formRegla.Estrato.CodigoEstrato
    }
    this.reglasService.postcrear(req).subscribe(
      (result) => {
        console.log(result);
        this.obtenerReglas();
        this.closeModal.nativeElement.click();
      }
    );
  }

  compareActividad( act1: any, act2: any): boolean {
    return act1 && act2 ? act1.DESCR_CORTA === act2.DESCR_CORTA : act1 === act2;
  }

  compareEstratos( e1: any, e2: any): boolean {
    return e1 && e2 ? e1.CodigoEstrato === e2.CodigoEstrato : e1 === e2;
  }

  compareHallazgos( h1: any, h2: any): boolean {
    return h1 && h2 ? h1.LlaveAnomalias === h2.LlaveAnomalias : h1 === h2;
  }

  compareAnomalias( h1: any, h2: any): boolean {
    return h1 && h2 ? h1.NombreAnomalias === h2.NombreAnomalias : h1 === h2;
  }

  onChangeHallazgoA() {
    console.log(this.TipoAnomaliaA);
    if( this.TipoAnomaliaA.TipoAnomalias !== 'Sin Definición') {
      this.reglasService.getHallazgosAnomalias( this.TipoAnomaliaA.TipoAnomalias).subscribe(
        (result) => {
          this.AnomaliasAList = result;
          this.formRegla.HallazgoA = result[0];
        }
      );
    }
  }

  onChangeHallazgoB() {
    // console.log(this.TipoAnomaliaA);
    if( this.TipoAnomaliaB.TipoAnomalias !== 'Sin Definición') {
      this.reglasService.getHallazgosAnomalias( this.TipoAnomaliaB.TipoAnomalias).subscribe(
        (result) => {
          this.AnomaliasBList = result;
          this.formRegla.HallazgoB = result[0];
        }
      );
    }
  }
}
