import { Component, OnInit } from '@angular/core';
import { ReglasService } from '../services/reglas.service';
import { RutasService } from '../services/rutas.service';

@Component({
  selector: 'app-rutas',
  templateUrl: './rutas.component.html',
  styleUrls: ['./rutas.component.css']
})
export class RutasComponent implements OnInit {

  ReglasList = [];
  Regla: any;
  PuntosList = [];
  todosSeleccionados = false;
  PuntoPre: any;
  
  reglaLoading = false;
  enviarLoading = false;

  constructor(
    private reglasService: ReglasService,
    private rutasService:RutasService
  ) { }

  ngOnInit(): void {
    this.reglasService.getReglas().subscribe(
      (result) => {
        this.ReglasList = result;
      }
    );
  }

  onChangeRegla() {
    this.reglaLoading = true;
    // console.log(this.Regla);
    this.PuntoPre = null;
    this.rutasService.getPuntosByRegla(this.Regla.NombreRegla).subscribe(
      (result) => {
        this.PuntosList = result;
        this.reglaLoading = false;
      },
      (error) => {
        console.error(error);
        this.reglaLoading = true;
      }
    );
  }

  enviarRuta() {
    // console.log('Enviar Ruta', this.PuntosList);
    this.enviarLoading = true;
    let arrayRequest = [];
    if( this.PuntosList.length > 0 ) {
      this.PuntosList.forEach( (p) => {
        if(p.seleccionado) {
          let objRequest = {
            CodigoPunto: p.CodigoPuntoConsumo,
            Regla: this.Regla.NombreRegla
          }

          arrayRequest.push(objRequest);
        }
      });

      if(arrayRequest.length > 0 ) {
        this.rutasService.postEnviarRuta(arrayRequest).subscribe(
          (result) => {
            console.log(result);
            this.enviarLoading = false;
          },
          (error) => {
            console.error(error);
            this.enviarLoading = false;
          }
        )
      }
    }
  }

  seleccionarTodos() {
    console.log(this.todosSeleccionados);
    if(this.PuntosList.length > 0) {
      this.PuntosList.forEach((p) => {
        p.seleccionado = this.todosSeleccionados;
      });
    }
  }

  closePuntosModal() {
    this.PuntoPre = null;  
  }

  showPreseleccion(punto: any) {
    this.PuntoPre = punto;
  }

  compareRegla( r1: any, r2: any): boolean {
    return r1 && r2 ? r1.NombreRegla === r2.NombreRegla : r1 === r2;
  }

}
