import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {EmployeeComponent} from './employee/employee.component';
import {DepartmentComponent} from './department/department.component';
import { RutasComponent } from './rutas/rutas.component';
import { ReglasComponent } from './reglas/reglas.component';
import { CampanasComponent } from './campanas/campanas.component';


const routes: Routes = [
  {path:'employee',component:EmployeeComponent},
  // {path:'department',component:DepartmentComponent},
  {path:'reglas',component:ReglasComponent},
  {path:'rutas',component:RutasComponent},
  {path:'campanas',component:CampanasComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
