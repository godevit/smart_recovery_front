import { Component, OnInit } from '@angular/core';
import { CampanasService } from '../services/campanas.service';

@Component({
  selector: 'app-campanas',
  templateUrl: './campanas.component.html',
  styleUrls: ['./campanas.component.css']
})
export class CampanasComponent implements OnInit {

  CampanasList = [];
  PuntosList = [];
  Campana: any;
  PuntosCampanaList = [];
  PuntoPre: any;
  CampanaAgregar: any;

  public lat = 51.673858;
	public lng = 7.815982;

  campanaLoading = false;

  constructor(
    private campService: CampanasService
  ) { }

  ngOnInit(): void {
    this.campService.getCampanas().subscribe(
      ( result ) => {
        this.CampanasList = result
      }
    );

    this.campService.getCodigoRegla().subscribe(
      (result) => {
        this.PuntosList = result;
        console.log(this.PuntosList);
      }
    );
  }

  onChangeCampana() {
    this.campanaLoading = true;
    console.log(this.Campana);
    this.campService.getPuntosCampana( this.Campana.NombreCapanaOrigen).subscribe(
      (result) => {
        this.PuntosCampanaList = result.slice(0,10);
        console.log(this.PuntosCampanaList);
        this.campanaLoading = false;
      },
      (error) => {
        console.error(error);
        this.campanaLoading = false;
      }
    );
  }

  closePuntosModal() {
    this.PuntoPre = null;  
  }

  showPreseleccion(punto: any) {
    this.PuntoPre = punto;
  }

  agregarACampana() {
    console.log(this.CampanaAgregar, this.PuntoPre);
    let req = {
      PuntoConsumo: this.PuntoPre.CodigoPuntoConsumo,
      Campana: this.CampanaAgregar.NombreCapanaOrigen
    }
    this.campService.postAgregarCampana(req).subscribe(
      (result) => {
        console.log(result);
        this.PuntoPre = null;
      }
    );
  }

  eliminarPunto( punto: any, index: number ) {
    // console.log(this.PuntosCampanaList[index]);
    
    this.campService.deletePunto(punto.CodigoPuntoConsumo).subscribe(
      (result)=>{
        this.PuntosCampanaList.splice(index, 1);
      }
    );
  }
}
