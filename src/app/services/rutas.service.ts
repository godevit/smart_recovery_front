import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RutasService {

  constructor(
    private http: HttpClient
  ) { }

  getPuntosByRegla( regla: string ) {
    return this.http.get<any>(environment.apiUrl + `/rutas?regla=${regla}`);
  }

  getPreseleccionPuntos( punto: string) {
    return this.http.get<any>(environment.apiUrl + `/rutas/preseleccion?PuntoConsumo=${punto}`);
  }

  postEnviarRuta( req: any) {
    return this.http.post<any>(environment.apiUrl + '/rutas', req);
  }
}
