import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReglasService {

  constructor(
    private http:HttpClient
  ) { }

  getEstratos() {
    return this.http.get<any>(environment.apiUrl + "/estratos");
  }

  getActividades() {
    return this.http.get<any>(environment.apiUrl + "/actividades");
  }

  getReglas() {
    return this.http.get<any>(environment.apiUrl + "/reglas");
  }

  getHallazgos() {
    return this.http.get<any>(environment.apiUrl + "/hallazgos");
  }

  postcrear(request: any) {
    return this.http.post<any>(environment.apiUrl + "/reglas", request);
  }

  postGeneracionAutomatica(request: any) {
    return this.http.post<any>(environment.apiUrl + "/reglas/generacion", request);
  }

  putGenerar(request: any) {
    return this.http.put<any>(environment.apiUrl + "/reglas/generada", request);
  }

  deleteRegla( regla: string ) {
    return this.http.delete<any>(environment.apiUrl + "/reglas?nombreRegla=" + regla);
  }

  getHallazgosTipoAnomalias() {
    return this.http.get<any>(environment.apiUrl + "/hallazgos/tipoanomalias");
  }

  getHallazgosAnomalias( tipoAnomalias: string) {
    return this.http.get<any>(environment.apiUrl + "/hallazgos/anomalias?tipoAnomalias=" + tipoAnomalias);
  }
}
