import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CampanasService {

  constructor(
    private http: HttpClient
  ) { }

  getCampanas() {
    return this.http.get<any>(environment.apiUrl + '/campanas');
  }

  getCodigoRegla() {
    return this.http.get<any>(environment.apiUrl + '/campanas/codigoregla');
  }

  getPuntosCampana( campana: string ) {
    return this.http.get<any>(environment.apiUrl + '/campanas/detail?Campana=' + campana);
  }
  
  postAgregarCampana(req: any) {
    return this.http.post<any>(environment.apiUrl + '/campanas', req);
  }

  deletePunto( punto: string ) {
    return this.http.delete<any>(environment.apiUrl + "/campanas?punto=" + punto);
  }
}
